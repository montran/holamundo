<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Manage Group Discount</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <spring:url value="/resources/css/style.css" var="styleCSS" />
  <link href="${styleCSS}" rel="stylesheet" />


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <spring:url value="/resources/js/groupd.js" var="crudJS" />
  <script src="${crudJS}"></script>

</head>

<body>
<%@include file="./header.jsp" %>
  <div class="container">
  <%@include file="./message.jsp" %>
    <div class="table-wrapper">
      <div class="table-title">
        <div class="row">
          <div class="col-sm-6">
            <h2>Manage <b>Group Discount</b></h2>
          </div>
          <div class="col-sm-6">
            <a href="#addModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add</span></a>
          </div>
        </div>
      </div>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Percentage</th>
            <th>Start Date</th>
            <th>End Date</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="disc" items="${list}">
            <tr>
              <td>${disc.id}</td>
              <td>${disc.name}</td>
              <td>${disc.percentage}</td>
               <td>${disc.startDate}</td>
               <td>${disc.endDate}</td>
              <td class="text-right">
                <a href="#editModal" data-id="${disc.id}" data-name="${disc.name}" data-percentage="${disc.percentage}" data-startdate="${disc.startDate}" data-enddate="${disc.endDate}" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                <a href="#deleteModal" data-id="${disc.id}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
              </td>
            </tr>
          </c:forEach>

        </tbody>
      </table>
    </div>
  </div>

  <!-- Add Modal HTML -->
  <div id="addModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="addForm" action="/HelloWorld/groupd/add/" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Add Group Discount</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label>Name</label>
              <input type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Percentage</label>
              <input type="number" step="any" name="percentage" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Start Date</label>
              <input type="date" name="startDate" class="form-control" required>
            </div>
            <div class="form-group">
              <label>End Date</label>
              <input type="date" name="endDate" class="form-control" required>
            </div>


          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="addButton" type="submit" class="btn btn-success" value="Add">
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Edit Modal HTML -->
  <div id="editModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="updateForm" action="#" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Edit Group Discount</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-group" style="display: none">
              <label>Id</label>
              <input id="updateId" type="text" name="id" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Name</label>
              <input id="updateName" type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Percentage</label>
              <input id="updatePercentage" type="number" step="any" name="percentage" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Start Date</label>
              <input id="updateStartDate" type="date" name="startDate" class="form-control" required>
            </div>
            <div class="form-group">
              <label>End Date</label>
              <input id="updateEndDate" type="date" name="endDate" class="form-control" required>
            </div>

          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="updateButton" type="submit" class="btn btn-info" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Delete Modal HTML -->
  <div id="deleteModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="deleteForm" action="#">
          <div class="modal-header">
            <h4 class="modal-title">Delete Group Discount</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete these Records?</p>
            <p class="text-warning"><small>This action cannot be undone.</small></p>
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input type="submit" class="btn btn-danger" value="Delete" name="objectId">
          </div>
        </form>
      </div>
    </div>
  </div>
</body>

</html>