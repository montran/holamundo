
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h1>Employees List</h1>
<table border="2" width="70%" cellpadding="2">
	<tr>
		<th>Id</th>
		<th>Apellido</th>
		<th>Nombre</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
	<c:forEach var="emp" items="${list}">
		<tr> 
			<td>${emp.codigo}</td>
			<td>${emp.apellido}</td>
			<td>${emp.nombre}</td>
			<td><a href="editForm/${emp.codigo}">Edit</a></td>
    		<td><a href="delete/${emp.codigo}">Delete</a></td>
		</tr>
	</c:forEach>
</table>
<br />
<a href="home">Add New Employee</a>