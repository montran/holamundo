<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Manage Currency</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <spring:url value="/resources/css/style.css" var="styleCSS" />
  <link href="${styleCSS}" rel="stylesheet" />


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <spring:url value="/resources/js/currency.js" var="crudJS" />
  <script src="${crudJS}"></script>

</head>

<body>
<%@include file="./header.jsp" %>
  <div class="container">
  <%@include file="./message.jsp" %>
    <div class="table-wrapper">
      <div class="table-title">
        <div class="row">
          <div class="col-sm-6">
            <h2>Manage <b>Currency</b></h2>
          </div>
          <div class="col-sm-6">
            <a href="#addModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add</span></a>
          </div>
        </div>
      </div>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Symbol</th>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="curr" items="${list}">
            <tr>
              <td>${curr.id}</td>
              <td>${curr.symbol}</td>
              <td>${curr.name}</td>
              <td class="text-right">
                <a href="#editModal" data-id="${curr.id}" data-symbol="${curr.symbol}" data-name="${curr.name}" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                <a href="#deleteModal" data-id="${curr.id}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
              </td>
            </tr>
          </c:forEach>

        </tbody>
      </table>
    </div>
  </div>

  <!-- Add Modal HTML -->
  <div id="addModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="addForm" action="/HelloWorld/currency/add/" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Add Currency</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label>Symbol</label>
              <input type="text" name="symbol" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Name</label>
              <input type="text" name="name" class="form-control" required>
            </div>

          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="addButton" type="submit" class="btn btn-success" value="Add">
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Edit Modal HTML -->
  <div id="editModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="updateForm" action="#" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Edit Currency</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-group" style="display: none">
              <label>Id</label>
              <input id="updateId" type="text" name="id" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Symbol</label>
              <input id="updateSymbol" type="text" name="symbol" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Name</label>
              <input id="updateName" type="text" name="name" class="form-control" required>
            </div>
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="updateButton" type="submit" class="btn btn-info" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Delete Modal HTML -->
  <div id="deleteModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="deleteForm" action="#">
          <div class="modal-header">
            <h4 class="modal-title">Delete Currency</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete these Records?</p>
            <p class="text-warning"><small>This action cannot be undone.</small></p>
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input type="submit" class="btn btn-danger" value="Delete" name="objectId">
          </div>
        </form>
      </div>
    </div>
  </div>
</body>

</html>