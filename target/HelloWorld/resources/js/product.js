$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
				$("#deleteForm").attr("action","/HelloWorld/product/delete/"+objectId);
    });

    $(".totalDisc").click(function(){
				var objectId = $(this).data("id");
			  $("#compositeId").attr("value",objectId);
    });


    $("#compositeButton").click(function(){
				var objectId =$("#compositeId").val();
        var quantity = $("#compoisteQuantity").val();
				$("#compositeForm").attr("action","/HelloWorld/product/discount/value/"+objectId+"/"+quantity);
    });

		$(".edit").click(function(){
				var productId = $(this).data("id");
				var productShortName = $(this).data("shortname");
				var productLongName = $(this).data("longname");
        var productPrice = $(this).data("price");
        var productStock = $(this).data("stock");
        var currencyId = $(this).data("currency");
        var groupId = $(this).data("group");
        var productKeywords = $(this).data("keywords");

        $("#updateId").attr("value",productId);
				$("#updateShortName").attr("value",productShortName);
				$("#updateLongName").attr("value",productLongName);
        $("#updatePrice").attr("value",productPrice);
        $("#updateStock").attr("value",productStock);
        $("#updateKeywords").attr("value",productKeywords);

        $('#updateCurrency').val(currencyId);
        $('#updateGroup').val(groupId);
    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/product/update/");
    });


});
