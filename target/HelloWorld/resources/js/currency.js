$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
				$("#deleteForm").attr("action","/HelloWorld/currency/delete/"+objectId);
    });

		$(".edit").click(function(){
				var currencyId = $(this).data("id");
				var currencySymbol = $(this).data("symbol");
				var currencyName = $(this).data("name");
				$("#updateId").attr("value",currencyId);
				$("#updateSymbol").attr("value",currencySymbol);
				$("#updateName").attr("value",currencyName);
    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/currency/update/");
    });


});
