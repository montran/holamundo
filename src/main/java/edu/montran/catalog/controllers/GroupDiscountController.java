package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.discount.GroupDiscount;
import edu.montran.catalog.manager.DiscountManager;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class GroupDiscountController.
 */
@Controller
public class GroupDiscountController {

	/** The discount manager. */
	@Autowired
	private DiscountManager discountManager;

	/**
	 * Group discount.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/groupd")
	public String groupDiscount(Model model) {

		List<GroupDiscount> discounts = discountManager.getAllGroupDiscounts();
		model.addAttribute("list", discounts);

		return "groupd";
	}

	/**
	 * Adds the volume discount.
	 *
	 * @param discount the discount
	 * @return the string
	 */
	@PostMapping(value = "/groupd/add/")
	public String addVolumeDiscount(GroupDiscount discount) {

		discountManager.addGroupDiscount(discount);

		return "redirect:/groupd";
	}

	/**
	 * Delete volume discount.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/groupd/delete/{id}")
	public String deleteVolumeDiscount(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			discountManager.removeDiscount(id);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/groupd";
	}

	/**
	 * Update currency.
	 *
	 * @param discount the discount
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/groupd/update/")
	public String updateCurrency(GroupDiscount discount, RedirectAttributes rattribs) {

		try {
			discountManager.editGroupDiscount(discount);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/groupd";
	}

}
