package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Group;

/**
 * The Class GroupController.
 */
@Controller
public class GroupController {

	/** The stock manager. */
	@Autowired
	private StockManager stockManager;

	/**
	 * Adds the group.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/group")
	public String addGroup(Model model) {

		List<Group> groups = stockManager.getAllGroups();
		model.addAttribute("list", groups);

		return "group";
	}

	/**
	 * Update group.
	 *
	 * @param group the group
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/group/update/")
	public String updateGroup(Group group, RedirectAttributes rattribs) {

		try {
			stockManager.updateGroup(group);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/group";
	}

	/**
	 * Delete group.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/group/delete/{id}")
	public String deleteGroup(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			stockManager.removeGroup(id);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/group";
	}

	/**
	 * Adds the group.
	 *
	 * @param group the group
	 * @return the string
	 */
	@PostMapping(value = "/group/add/")
	public String addGroup(Group group) {

		stockManager.addGroup(group);

		return "redirect:/group";
	}

}
