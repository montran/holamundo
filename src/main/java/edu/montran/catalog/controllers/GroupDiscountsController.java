package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.discount.GroupDiscount;
import edu.montran.catalog.manager.DiscountManager;
import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Group;

/**
 * The Class GroupDiscountsController.
 */
@Controller
public class GroupDiscountsController {

	/** The stock manager. */
	@Autowired
	private StockManager stockManager;

	/** The discount manager. */
	@Autowired
	private DiscountManager discountManager;

	/**
	 * Discount.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/group/discount/{id}")
	public String discount(@PathVariable long id, Model model) {

		Group group = stockManager.findGroupById(id);
		List<GroupDiscount> discounts = group.getDiscounts();

		model.addAttribute("list", discounts);
		model.addAttribute("groupId", id);

		List<GroupDiscount> availableDiscounts = discountManager.getAllGroupDiscounts();
		model.addAttribute("discounts", availableDiscounts);

		return "group-discounts";
	}

	/**
	 * Delete product discount.
	 *
	 * @param discountId the discount id
	 * @param groupId the group id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/group/discount/delete/{discountId}/{groupId}")
	public String deleteProductDiscount(@PathVariable long discountId, @PathVariable long groupId,
			RedirectAttributes rattribs) {

		try {
			discountManager.removeDiscountFromGroup(discountId, groupId);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/group/discount/" + groupId;
	}

	/**
	 * Adds the product discount.
	 *
	 * @param groupId the group id
	 * @param discountId the discount id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/group/discount/add/")
	public String addProductDiscount(@RequestParam("id") long groupId, @RequestParam("discount") long discountId,
			RedirectAttributes rattribs) {

		try {
			discountManager.addDiscountToGroup(discountId, groupId);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error",
					e.toString() + ", it looks you are trying to add multiple times the same discount");
		}

		return "redirect:/group/discount/" + groupId;
	}

}
