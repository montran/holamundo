package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Atribute;
import edu.montran.catalog.stock.Product;

/**
 * The Class AtributeController.
 */
@Controller
public class AtributeController {

	/** The stock manager. */
	@Autowired
	private StockManager stockManager;

	/**
	 * Attribute.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/attribute/{id}")
	public String attribute(@PathVariable long id, Model model) {

		List<Atribute> attributes = stockManager.getProductAttributes(id);
		model.addAttribute("list", attributes);
		model.addAttribute("productId", id);

		return "attribute";
	}

	/**
	 * Creates the attribute.
	 *
	 * @param attribute the attribute
	 * @param productId the product id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/attribute/add")
	public String createAttribute(Atribute attribute, @RequestParam("prodid") long productId,
			RedirectAttributes rattribs) {

		try {
			stockManager.addAtribute(attribute, productId);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/attribute/" + productId;
	}

	/**
	 * Update product.
	 *
	 * @param id the id
	 * @param productId the product id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/attribute/delete/{id}/{productId}")
	public String updateProduct(@PathVariable long id, @PathVariable long productId, RedirectAttributes rattribs) {

		try {
			stockManager.removeAtribute(id);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/attribute/" + productId;
	}

	/**
	 * Update attribute.
	 *
	 * @param attribute the attribute
	 * @param productId the product id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/attribute/update")
	public String updateAttribute(Atribute attribute, @RequestParam("prodid") long productId,
			RedirectAttributes rattribs) {

		try {
			Product product = stockManager.findProductById(productId);
			attribute.setProduct(product);
			stockManager.updateAttribute(attribute);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/attribute/" + productId;
	}

}
