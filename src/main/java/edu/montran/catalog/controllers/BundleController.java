package edu.montran.catalog.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Bundle;
import edu.montran.catalog.stock.Product;
import edu.montran.catalog.stock.ProductsPerBundle;

/**
 * The Class BundleController.
 */
@Controller
public class BundleController {

	/** The stock manager. */
	@Autowired
	private StockManager stockManager;

	/** The Constant Log. */
	private static final Logger Log = LogManager.getLogger(StockManager.class);

	/**
	 * Bundle.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/bundle")
	public String bundle(Model model) {

		List<Bundle> bundles = stockManager.getAllBundles();
		model.addAttribute("list", bundles);

		List<Product> products = stockManager.getAllProducts();
		model.addAttribute("productList", products);

		List<String> bundleProductsList = new ArrayList<>();

		for (int i = 0; i < bundles.size(); i++) {

			String bundleProducts = "";

			List<ProductsPerBundle> productsBundles = stockManager.getProductsPerBundle(bundles.get(i).getId());

			for (int j = 0; j < productsBundles.size(); j++) {
				Product aux = productsBundles.get(j).getProduct();
				bundleProducts += aux.getId() + "->" + aux.getLongName() + "->" + productsBundles.get(j).getQuantity()
						+ "->";
			}

			bundleProductsList.add(bundleProducts);
		}

		model.addAttribute("bundleProducts", bundleProductsList);

		return "bundle";
	}

	/**
	 * Delete bundle.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/bundle/delete/{id}")
	public String deleteBundle(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			stockManager.removeBundle(id);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/bundle";
	}

	/**
	 * Update bundle.
	 *
	 * @param id the id
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/bundle/update/")
	public String updateBundle(@RequestParam("id") long id, @RequestParam("shortName") String shortName,
			@RequestParam("longName") String longName, @RequestParam("price") float price,
			RedirectAttributes rattribs) {

		try {
			stockManager.updateBundle(id, shortName, longName, price);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/bundle";
	}

	/**
	 * Adds the bundle.
	 *
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 * @return the string
	 */
	@PostMapping(value = "/bundle/add")
	public String addBundle(@RequestParam("shortName") String shortName, @RequestParam("longName") String longName,
			@RequestParam("price") float price) {

		stockManager.addBundle(shortName, longName, price);

		return "redirect:/bundle";
	}

	/**
	 * Adds the product.
	 *
	 * @param postPayload the post payload
	 * @param productId the product id
	 * @param quantity the quantity
	 * @param bundleId the bundle id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/bundle/product/add")
	public String addProduct(@RequestBody String postPayload, @RequestParam(value = "productid") long[] productId,
			@RequestParam(value = "quantity") int[] quantity, @RequestParam(value = "id") long bundleId,
			RedirectAttributes rattribs) {

		Log.info("Post data: " + postPayload);

		Log.info("Bundle id " + bundleId);

		Bundle bundle = stockManager.findBundleById(bundleId);

		List<ProductsPerBundle> productsPerBundle = bundle.getProductsPerBundle();

		if (!productsPerBundle.isEmpty()) {
			bundle.clearProductsPerBundle();
			try {
				stockManager.updateBundle(bundle);
			} catch (PersistanceException e) {
				rattribs.addFlashAttribute("error", e.toString());
			}
		}

		for (int i = 0; i < productId.length; i++) {
			Log.info("Product id " + productId[i] + " quantity " + quantity[i]);
			try {
				stockManager.addProductToBundle(productId[i], bundleId, quantity[i]);
			} catch (PersistanceException e) {
				rattribs.addFlashAttribute("error", e.toString());
			}
		}

		return "redirect:/bundle";
	}

}
