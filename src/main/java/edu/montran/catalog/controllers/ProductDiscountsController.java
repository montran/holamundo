package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.discount.AbstractDiscount;
import edu.montran.catalog.manager.DiscountManager;
import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Product;

/**
 * The Class ProductDiscountsController.
 */
@Controller
public class ProductDiscountsController {
	
	/** The stock manager. */
	@Autowired
	private StockManager stockManager;
	
	/** The discount manager. */
	@Autowired
	private DiscountManager discountManager;

	/**
	 * Discount.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/product/discount/{id}")
	public String discount(@PathVariable long id, Model model) {

		Product product = stockManager.findProductById(id);
		List<AbstractDiscount> discounts = product.getDiscounts();

		model.addAttribute("list", discounts);
		model.addAttribute("productId", id);

		List<AbstractDiscount> availableDiscounts = discountManager.getAllDiscounts();
		model.addAttribute("discounts", availableDiscounts);

		return "product-discounts";
	}

	/**
	 * Delete product discount.
	 *
	 * @param discountId the discount id
	 * @param productId the product id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/product/discount/delete/{discountId}/{productId}")
	public String deleteProductDiscount(@PathVariable long discountId, @PathVariable long productId,
			RedirectAttributes rattribs) {

		try {
			discountManager.removeDiscountFromProduct(discountId, productId);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/product/discount/" + productId;
	}

	/**
	 * Adds the product discount.
	 *
	 * @param productId the product id
	 * @param discountId the discount id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/product/discount/add/")
	public String addProductDiscount(@RequestParam("id") long productId, @RequestParam("discount") long discountId,
			RedirectAttributes rattribs) {

		try {
			discountManager.addDiscountToProduct(discountId, productId);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString()+", it looks you are trying to add multiple times the same discount");
		}

		return "redirect:/product/discount/" + productId;
	}

}
