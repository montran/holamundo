package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.discount.VolumeDiscount;
import edu.montran.catalog.manager.DiscountManager;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class VolumeDiscountController.
 */
@Controller
public class VolumeDiscountController {

	/** The discount manager. */
	@Autowired
	private DiscountManager discountManager;

	/**
	 * Age discount.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/volume")
	public String ageDiscount(Model model) {

		List<VolumeDiscount> discounts = discountManager.getAllVolumeDiscounts();
		model.addAttribute("list", discounts);

		return "volume";
	}

	/**
	 * Adds the volume discount.
	 *
	 * @param discount the discount
	 * @return the string
	 */
	@PostMapping(value = "/volume/add/")
	public String addVolumeDiscount(VolumeDiscount discount) {

		discountManager.addVolumeDiscount(discount);

		return "redirect:/volume";
	}

	/**
	 * Delete volume discount.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/volume/delete/{id}")
	public String deleteVolumeDiscount(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			discountManager.removeDiscount(id);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/volume";
	}

	/**
	 * Update currency.
	 *
	 * @param discount the discount
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/volume/update/")
	public String updateCurrency(VolumeDiscount discount, RedirectAttributes rattribs) {

		try {
			discountManager.editVolumeDiscount(discount);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/volume";
	}

}
