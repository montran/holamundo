package edu.montran.catalog.controllers;

import java.time.LocalDate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import edu.montran.catalog.discount.GroupDiscount;
import edu.montran.catalog.manager.CurrencyManager;
import edu.montran.catalog.manager.DiscountManager;
import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class HomeController.
 */
//https://www.javatpoint.com/spring-mvc-crud-example
@Controller
public class HomeController {

	/** The currency manager. */
	@Autowired
	private CurrencyManager currencyManager;

	/** The stock manager. */
	@Autowired
	private StockManager stockManager;

	/** The discount manager. */
	@Autowired
	private DiscountManager discountManager;

	/** The Constant Log. */
	private static final Logger Log = LogManager.getLogger(CurrencyManager.class);
	
	/** The Constant ERROR_MESSAGE. */
	public static final String ERROR_MESSAGE = "Error creating example ";

	/**
	 * Index.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/")
	public String index(Model model) {

		return "redirect:/product";
	}

	/**
	 * Demo.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/demo")
	public String demo(Model model) {

		return "home";
	}

	/**
	 * Main.
	 *
	 * @return the string
	 */
	@PostMapping(value = "/main")
	public String main() {

		// Currency Test
		long currency1Id = currencyManager.addCurrency("&#36;", "Dolar");
		currencyManager.addCurrency("&euro;", "Euro");

		long group1Id = stockManager.addGroup("Electronics", "Lorem ipsum");
		stockManager.addGroup("Computers", "Lorem ipsum");

		long product2Id = stockManager.addProduct("TV01", "LG Television", 400, 10, currency1Id, group1Id, "#fhd");
		stockManager.addProduct("TV02", "Sony Television", 500, 10, currency1Id, group1Id, "#new");
		stockManager.addProduct("TV03", "Samsung Television", 450, 10, currency1Id, group1Id, "#hd");
		stockManager.addProduct("TV04", "Sanyo Television", 470, 15, currency1Id, group1Id, "#hd");

		try {
			stockManager.addAtribute(product2Id, "String", "Condition", "New");
		} catch (PersistanceException e) {

			Log.info(ERROR_MESSAGE + e.getMessage());
		}

		long bundleId = stockManager.addBundle("TVB01", "LG Television Bundle", 400);

		try {
			stockManager.addProductToBundle(product2Id, bundleId, 2);
		} catch (PersistanceException e) {
			Log.info(ERROR_MESSAGE + e.getMessage());
		}

		long discId = discountManager.addVolumeDiscount("Volume Promo 1", 10, 20, LocalDate.now().toString(),
													     LocalDate.now().toString(), 0.1f);

		long ageId = discountManager.addAgeDiscount("Age Promo 1",LocalDate.now().toString(), LocalDate.now().toString(),
				LocalDate.now().toString(), 0.20f);

		try {
			discountManager.addDiscountToProduct(discId, product2Id);
		} catch (PersistanceException e) {
			Log.info(ERROR_MESSAGE + e.getMessage());
		}

		try {
			discountManager.addDiscountToProduct(ageId, product2Id);
		} catch (PersistanceException e) {
			Log.info(ERROR_MESSAGE + e.getMessage());
		}

		try {
			discountManager.editVolumeDiscount(discId, 0.2f, 10, 20);
		} catch (PersistanceException e) {
			Log.info(ERROR_MESSAGE + e.getMessage());
		}

		GroupDiscount groupDiscount = new GroupDiscount();

		groupDiscount.setEndDate(LocalDate.now());
		groupDiscount.setName("Group Promo 1");
		groupDiscount.setPercentage(0.1f);
		groupDiscount.setStartDate(LocalDate.now());

		long discountId = discountManager.addGroupDiscount(groupDiscount);

		try {
			discountManager.addDiscountToGroup(discountId, group1Id);
		} catch (PersistanceException e) {
			Log.info(ERROR_MESSAGE + e.getMessage());
		}

		stockManager.getProductTotalDicount(product2Id, 10);

		return "redirect:/";
	}

}
