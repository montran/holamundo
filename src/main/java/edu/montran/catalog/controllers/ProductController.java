package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.manager.CurrencyManager;
import edu.montran.catalog.manager.StockManager;
import edu.montran.catalog.money.Currency;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Group;
import edu.montran.catalog.stock.Product;

/**
 * The Class ProductController.
 */
@Controller
public class ProductController {
	
	/** The currency manager. */
	@Autowired
	private CurrencyManager currencyManager;
	
	/** The stock manager. */
	@Autowired
	private StockManager stockManager;

	/**
	 * Product.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/product")
	public String product(Model model) {

		List<Product> products = stockManager.getAllProducts();
		model.addAttribute("productList", products);

		List<Currency> currencies = currencyManager.getAllCurrecies();
		model.addAttribute("currencyList", currencies);

		List<Group> groups = stockManager.getAllGroups();
		model.addAttribute("groupList", groups);

		return "product";
	}

	/**
	 * Update product.
	 *
	 * @param id the id
	 * @param price the price
	 * @param stock the stock
	 * @param shortName the short name
	 * @param longName the long name
	 * @param groupId the group id
	 * @param keywords the keywords
	 * @param currencyId the currency id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/product/update")
	public String updateProduct(@RequestParam("id") long id, @RequestParam("price") float price,
			@RequestParam("stock") int stock, @RequestParam("shortName") String shortName,
			@RequestParam("longName") String longName, @RequestParam("group") long groupId,
			@RequestParam("keywords") String keywords, @RequestParam("currency") long currencyId,
			RedirectAttributes rattribs) {

		Currency currency = currencyManager.findCurrencyById(currencyId);
		Group group = stockManager.findGroupById(groupId);
		Product product = stockManager.findProductById(id);

		product.setCurrency(currency);
		product.setGroup(group);
		product.setLongName(longName);
		product.setShortName(shortName);
		product.setPrice(price);
		product.setStock(stock);
		product.setKeywords(keywords);

		try {
			stockManager.updateProduct(product);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/product";
	}

	/**
	 * Update product.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/product/delete/{id}")
	public String updateProduct(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			stockManager.removeProduct(id);
		} catch (PersistanceException e) {
			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/product";
	}

	/**
	 * Gets the product discount percent.
	 *
	 * @param id the id
	 * @param quantity the quantity
	 * @param rattribs the rattribs
	 * @return the product discount percent
	 */
	@PostMapping(value = "/product/discount/value/{id}/{quantity}")
	public String getProductDiscountPercent(@PathVariable long id, @PathVariable int quantity,
			RedirectAttributes rattribs) {

		float value = stockManager.getProductTotalDicount(id, quantity);
		Product product = stockManager.findProductById(id);
		
		rattribs.addFlashAttribute("discount", "Total discount for "+quantity+" "+product.getLongName()+" is "+value);

		return "redirect:/product";
	}

	/**
	 * Creates the product.
	 *
	 * @param price the price
	 * @param stock the stock
	 * @param shortName the short name
	 * @param longName the long name
	 * @param keywords the keywords
	 * @param groupId the group id
	 * @param currencyId the currency id
	 * @return the string
	 */
	@PostMapping(value = "/product/add")
	public String createProduct(@RequestParam("price") float price, @RequestParam("stock") int stock,
			@RequestParam("shortName") String shortName, @RequestParam("longName") String longName,
			@RequestParam("keywords") String keywords, @RequestParam("group") long groupId,
			@RequestParam("currency") long currencyId) {

		Currency currency = currencyManager.findCurrencyById(currencyId);
		Group group = stockManager.findGroupById(groupId);
		Product product = new Product(shortName, longName, price, stock);

		product.setCurrency(currency);
		product.setGroup(group);
		product.setKeywords(keywords);

		stockManager.addProduct(product);

		return "redirect:/product";
	}

}