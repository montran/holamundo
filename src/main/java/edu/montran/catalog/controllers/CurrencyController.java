package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.manager.CurrencyManager;
import edu.montran.catalog.money.Currency;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class CurrencyController.
 */
@Controller
public class CurrencyController {

	/** The currency manager. */
	@Autowired
	private CurrencyManager currencyManager;

	/**
	 * Currency.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/currency")
	public String currency(Model model) {

		List<Currency> currencies = currencyManager.getAllCurrecies();
		model.addAttribute("list", currencies);

		return "currency";
	}

	/**
	 * Delete currency.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/currency/delete/{id}")
	public String deleteCurrency(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			currencyManager.removeCurrency(id);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/currency";
	}

	/**
	 * Update currency.
	 *
	 * @param currency the currency
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/currency/update/")
	public String updateCurrency(Currency currency, RedirectAttributes rattribs) {

		try {
			currencyManager.updateCurrency(currency);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/currency";
	}

	/**
	 * Adds the currency.
	 *
	 * @param currency the currency
	 * @return the string
	 */
	@PostMapping(value = "/currency/add/")
	public String addCurrency(Currency currency) {

		currencyManager.addCurrency(currency);

		return "redirect:/currency";
	}

}
