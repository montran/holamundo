package edu.montran.catalog.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.montran.catalog.discount.AgeDiscount;
import edu.montran.catalog.manager.DiscountManager;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class AgeDiscountController.
 */
@Controller
public class AgeDiscountController {

	/** The discount manager. */
	@Autowired
	private DiscountManager discountManager;

	/**
	 * Age discount.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/age")
	public String ageDiscount(Model model) {

		List<AgeDiscount> discounts = discountManager.getAllAgeDiscounts();
		model.addAttribute("list", discounts);

		return "age";
	}

	/**
	 * Adds the volume discount.
	 *
	 * @param discount the discount
	 * @return the string
	 */
	@PostMapping(value = "/age/add/")
	public String addVolumeDiscount(AgeDiscount discount) {

		discountManager.addAgeDiscount(discount);

		return "redirect:/age";
	}

	/**
	 * Delete volume discount.
	 *
	 * @param id the id
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@GetMapping(value = "/age/delete/{id}")
	public String deleteVolumeDiscount(@PathVariable long id, RedirectAttributes rattribs) {

		try {
			discountManager.removeDiscount(id);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/age";
	}

	/**
	 * Update currency.
	 *
	 * @param discount the discount
	 * @param rattribs the rattribs
	 * @return the string
	 */
	@PostMapping(value = "/age/update/")
	public String updateCurrency(AgeDiscount discount, RedirectAttributes rattribs) {

		try {
			discountManager.editAgeDiscount(discount);
		} catch (PersistanceException e) {

			rattribs.addFlashAttribute("error", e.toString());
		}

		return "redirect:/age";
	}

}
