package edu.montran.catalog.money;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.envers.Audited;

import edu.montran.catalog.stock.Product;

/**
 * The Class Currency.
 */
@Entity
@Table(name = "CURRENCY")
@Audited
public class Currency {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CURR_ID")
	private long id;

	/** The symbol. */
	@Column(name = "CURR_SYMBOL")
	String symbol;

	/** The name. */
	@Column(name = "CURR_NAME")
	String name;

	/** The products. */
	// @NotAudited
	@OneToMany(mappedBy = "currency", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	List<Product> products = new ArrayList<>();

	/**
	 * Adds the product.
	 *
	 * @param item the item
	 */
	public void addProduct(Product item) {
		products.add(item);
	}

	/**
	 * Instantiates a new currency.
	 *
	 * @param symbol the symbol
	 * @param name the name
	 */
	public Currency(String symbol, String name) {
		this.symbol = symbol;
		this.name = name;
	}

	/**
	 * Instantiates a new currency.
	 */
	public Currency() {
	}

	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Sets the symbol.
	 *
	 * @param symbol the new symbol
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Currency [id=" + id + ", symbol=" + symbol + ", name=" + name + ", products=" + products + "]";
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
