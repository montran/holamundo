package edu.montran.catalog.manager;

import java.time.LocalDate;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.montran.catalog.discount.AbstractDiscount;
import edu.montran.catalog.discount.CompositeDiscount;
import edu.montran.catalog.manager.utilities.Utilities;
import edu.montran.catalog.money.Currency;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Atribute;
import edu.montran.catalog.stock.Bundle;
import edu.montran.catalog.stock.Group;
import edu.montran.catalog.stock.Product;
import edu.montran.catalog.stock.ProductsPerBundle;

/**
 * The Class StockManager.
 */
@Component
public final class StockManager {

	
	/** The currency manager. */
	@Autowired
	private CurrencyManager currencyManager;
	
	/** The persistance manager. */
	@Autowired
	private PersistanceManager persistanceManager;

	/** The Constant Log. */
	private static final Logger Log = LogManager.getLogger(StockManager.class);

	/**
	 * Adds the group.
	 *
	 * @param name the name
	 * @param description the description
	 * @return the long
	 */
	public long addGroup(String name, String description) {

		Group group = new Group(name, description);
		Log.info(Utilities.INSERT_MESSAGE + group);
		persistanceManager.create(group);

		return group.getId();
	}

	/**
	 * Adds the group.
	 *
	 * @param group the group
	 * @return the long
	 */
	public long addGroup(Group group) {

		Log.info(Utilities.INSERT_MESSAGE + group);

		persistanceManager.create(group);

		return group.getId();
	}

	/**
	 * Removes the group.
	 *
	 * @param id the id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeGroup(long id) throws PersistanceException {

		Log.info(Utilities.DELETE_MESSAGE + Group.class);
		Log.info("Id: " + id);

		persistanceManager.delete(Group.class, id);
	}

	/**
	 * Update group.
	 *
	 * @param id the id
	 * @param name the name
	 * @param description the description
	 * @throws PersistanceException the persistance exception
	 */
	public void updateGroup(long id, String name, String description) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + Group.class);
		Log.info("Id: " + id);

		Group group = persistanceManager.findById(Group.class, id);
		group.setName(name);
		group.setDescription(description);
		persistanceManager.update(group);
	}

	/**
	 * Update group.
	 *
	 * @param group the group
	 * @throws PersistanceException the persistance exception
	 */
	public void updateGroup(Group group) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + group);
		Log.info("Id: " + group.getId());

		persistanceManager.update(group);
	}

	/**
	 * Find group by id.
	 *
	 * @param id the id
	 * @return the group
	 */
	public Group findGroupById(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Currency.class);
		Log.info("Id: " + id);

		return persistanceManager.findById(Group.class, id);
	}

	/**
	 * Gets the all groups.
	 *
	 * @return the all groups
	 */
	public List<Group> getAllGroups() {

		Log.info(Utilities.GET_ALL_MESSAGE + Group.class);

		return persistanceManager.findAll(Group.class);
	}

	/**
	 * Adds the product.
	 *
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 * @param stock the stock
	 * @param currencyId the currency id
	 * @param groupId the group id
	 * @param keywords the keywords
	 * @return the long
	 */
	public long addProduct(String shortName, String longName, float price, int stock, long currencyId, long groupId,
			String keywords) {

		Log.info(Utilities.INSERT_MESSAGE + Product.class);
		Log.info("Params: short name = " + shortName + "long name = " + longName + "price = " + price);

		Product product = new Product(shortName, longName, price, stock);

		Currency currency = currencyManager.findCurrencyById(currencyId);
		Group group = findGroupById(groupId);

		product.setCreationDate(LocalDate.now());
		product.setCurrency(currency);
		product.setGroup(group);
		product.setKeywords(keywords);

		persistanceManager.create(product);

		return product.getId();
	}

	/**
	 * Adds the product.
	 *
	 * @param product the product
	 * @return the long
	 */
	public long addProduct(Product product) {

		Log.info(Utilities.INSERT_MESSAGE + product);
		Log.info("Params: short name = " + product.getShortName() + "long name = " + product.getLongName() + "price = "
				+ product.getPrice());

		product.setCreationDate(LocalDate.now());

		persistanceManager.create(product);

		return product.getId();
	}

	/**
	 * Removes the product.
	 *
	 * @param id the id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeProduct(long id) throws PersistanceException {

		Log.info(Utilities.DELETE_MESSAGE + Product.class);
		Log.info("Id: " + id);

		persistanceManager.delete(Product.class, id);
	}

	/**
	 * Update product.
	 *
	 * @param productId the product id
	 * @param longName the long name
	 * @param shortName the short name
	 * @param price the price
	 * @throws PersistanceException the persistance exception
	 */
	public void updateProduct(long productId, String longName, String shortName, float price)
			throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + Currency.class);
		Log.info("Id: " + productId);

		Product product = persistanceManager.findById(Product.class, productId);
		product.setLongName(longName);
		product.setShortName(shortName);
		product.setPrice(price);
		persistanceManager.update(product);
	}

	/**
	 * Update product.
	 *
	 * @param product the product
	 * @throws PersistanceException the persistance exception
	 */
	public void updateProduct(Product product) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + Currency.class);
		Log.info("Id: " + product.getId());

		persistanceManager.update(product);
	}

	/**
	 * Find product by id.
	 *
	 * @param id the id
	 * @return the product
	 */
	public Product findProductById(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Currency.class);
		Log.info("Id: " + id);

		return persistanceManager.findById(Product.class, id);
	}

	/**
	 * Gets the all products.
	 *
	 * @return the all products
	 */
	public List<Product> getAllProducts() {

		Log.info(Utilities.GET_ALL_MESSAGE + Product.class);

		return persistanceManager.findAll(Product.class);
	}

	/**
	 * Adds the atribute.
	 *
	 * @param productId the product id
	 * @param type the type
	 * @param name the name
	 * @param value the value
	 * @return the long
	 * @throws PersistanceException the persistance exception
	 */
	public long addAtribute(long productId, String type, String name, String value) throws PersistanceException {

		Log.info(Utilities.INSERT_MESSAGE + Product.class);
		Log.info("Params: name = " + name + "type = " + type + " value = " + value);

		Product product = persistanceManager.findById(Product.class, productId);

		Atribute atribute = new Atribute(type, name, value);
		persistanceManager.create(atribute);

		atribute.setProduct(product);
		persistanceManager.update(atribute);

		return atribute.getId();
	}

	/**
	 * Adds the atribute.
	 *
	 * @param attribute the attribute
	 * @param productId the product id
	 * @return the long
	 * @throws PersistanceException the persistance exception
	 */
	public long addAtribute(Atribute attribute, long productId) throws PersistanceException {

		Log.info("Params: name = " + attribute.getName() + "type = " + attribute.getType() + " value = "
				+ attribute.getValue());

		Product product = persistanceManager.findById(Product.class, productId);

		persistanceManager.create(attribute);

		attribute.setProduct(product);
		persistanceManager.update(attribute);

		return attribute.getId();
	}

	/**
	 * Find atribute by id.
	 *
	 * @param id the id
	 * @return the atribute
	 */
	public Atribute findAtributeById(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Atribute.class);
		Log.info("Id: " + id);

		return persistanceManager.findById(Atribute.class, id);
	}

	/**
	 * Update attribute.
	 *
	 * @param attribute the attribute
	 * @throws PersistanceException the persistance exception
	 */
	public void updateAttribute(Atribute attribute) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + attribute);

		persistanceManager.update(attribute);
	}

	/**
	 * Adds the group to product.
	 *
	 * @param productId the product id
	 * @param groupId the group id
	 * @throws PersistanceException the persistance exception
	 */
	public void addGroupToProduct(long productId, long groupId) throws PersistanceException {

		Log.info("Adding group to product");
		Log.info("Params: Product Id = " + productId + "Group Id = " + groupId);

		Product product = persistanceManager.findById(Product.class, productId);

		persistanceManager.update(product);

	}

	/**
	 * Removes the atribute.
	 *
	 * @param id the id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeAtribute(long id) throws PersistanceException {

		Log.info("Removing an object of type: " + Atribute.class);
		Log.info("Id: " + id);

		persistanceManager.delete(Atribute.class, id);
	}

	/**
	 * Gets the product attributes.
	 *
	 * @param id the id
	 * @return the product attributes
	 */
	public List<Atribute> getProductAttributes(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Atribute.class);
		Log.info("Id: " + id);

		return persistanceManager.findByCriteria(Atribute.class, "product", id);
	}

	/**
	 * Adds the bundle.
	 *
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 * @return the long
	 */
	public long addBundle(String shortName, String longName, float price) {

		Log.info(Utilities.INSERT_MESSAGE + Bundle.class);
		Log.info("Params: short name = " + shortName + "long name = " + longName + " price = " + price);

		Bundle bundle = new Bundle(shortName, longName, price);

		persistanceManager.create(bundle);

		return bundle.getId();

	}

	/**
	 * Removes the bundle.
	 *
	 * @param id the id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeBundle(long id) throws PersistanceException {

		Log.info(Utilities.DELETE_MESSAGE + Bundle.class);
		Log.info("Id: " + id);

		persistanceManager.delete(Bundle.class, id);
	}

	/**
	 * Gets the all bundles.
	 *
	 * @return the all bundles
	 */
	public List<Bundle> getAllBundles() {

		Log.info(Utilities.GET_ALL_MESSAGE + Bundle.class);

		return persistanceManager.findAll(Bundle.class);
	}

	/**
	 * Update bundle.
	 *
	 * @param id the id
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 * @throws PersistanceException the persistance exception
	 */
	public void updateBundle(long id, String shortName, String longName, float price) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + Bundle.class);
		Log.info("Id: " + id);

		Bundle bundle = persistanceManager.findById(Bundle.class, id);
		bundle.setShortName(shortName);
		bundle.setLongName(longName);
		bundle.setPrice(price);
		persistanceManager.update(bundle);
	}

	/**
	 * Update bundle.
	 *
	 * @param bundle the bundle
	 * @throws PersistanceException the persistance exception
	 */
	public void updateBundle(Bundle bundle) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + bundle);

		persistanceManager.update(bundle);
	}

	/**
	 * Find bundle by id.
	 *
	 * @param id the id
	 * @return the bundle
	 */
	public Bundle findBundleById(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Bundle.class);
		Log.info("Id: " + id);

		return persistanceManager.findById(Bundle.class, id);
	}

	/**
	 * Adds the product to bundle.
	 *
	 * @param productId the product id
	 * @param bundleId the bundle id
	 * @param quantity the quantity
	 * @return the long
	 * @throws PersistanceException the persistance exception
	 */
	public long addProductToBundle(long productId, long bundleId, int quantity) throws PersistanceException {

		Log.info("Adding product to bundle");
		Log.info("Params: Product Id = " + productId + "Bundle Id = " + bundleId);

		Product product = persistanceManager.findById(Product.class, productId);
		Bundle bundle = persistanceManager.findById(Bundle.class, bundleId);

		ProductsPerBundle productPerBundle = new ProductsPerBundle();

		productPerBundle.setQuantity(quantity);

		persistanceManager.create(productPerBundle);

		productPerBundle.setProduct(product);
		productPerBundle.setBundle(bundle);

		persistanceManager.update(productPerBundle);

		return productPerBundle.getId();

	}

	/**
	 * Removes the product from bundle.
	 *
	 * @param productsPerBundleId the products per bundle id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeProductFromBundle(long productsPerBundleId) throws PersistanceException {

		Log.info("Removing group to bundle");
		Log.info("Params: Products per Bundle Id = " + productsPerBundleId);

		persistanceManager.delete(ProductsPerBundle.class, productsPerBundleId);

	}

	/**
	 * Gets the products per bundle.
	 *
	 * @param id the id
	 * @return the products per bundle
	 */
	public List<ProductsPerBundle> getProductsPerBundle(long id) {

		Log.info(Utilities.GET_ALL_MESSAGE + ProductsPerBundle.class);

		return persistanceManager.findByCriteria(ProductsPerBundle.class, "bundle", id);
	}

	/**
	 * Find products per bundle.
	 *
	 * @param id the id
	 * @return the products per bundle
	 */
	public ProductsPerBundle findProductsPerBundle(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Currency.class);
		Log.info("Id: " + id);

		return persistanceManager.findById(ProductsPerBundle.class, id);
	}

	/**
	 * Gets the product total dicount.
	 *
	 * @param productId the product id
	 * @param quantity the quantity
	 * @return the product total dicount
	 */
	public float getProductTotalDicount(long productId, int quantity) {
		Product product = findProductById(productId);

		List<AbstractDiscount> discounts = product.getDiscounts();

		CompositeDiscount composite = new CompositeDiscount();

		composite.setDiscounts(discounts);

		return composite.computeDiscount(product, quantity);

	}

}
