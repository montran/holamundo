package edu.montran.catalog.manager.utilities;

/**
 * The Class Utilities.
 */
public final class Utilities {

	/**
	 * Instantiates a new utilities.
	 */
	Utilities() {

	}

	/** The Constant INSERT_MESSAGE. */
	public static final String INSERT_MESSAGE = "Inserting an object: ";
	
	/** The Constant DELETE_MESSAGE. */
	public static final String DELETE_MESSAGE = "Removing an object: ";
	
	/** The Constant UPDATE_MESSAGE. */
	public static final String UPDATE_MESSAGE = "Updating an object: ";
	
	/** The Constant SEARCH_MESSAGE. */
	public static final String SEARCH_MESSAGE = "Looking for an object: ";
	
	/** The Constant GET_ALL_MESSAGE. */
	public static final String GET_ALL_MESSAGE = "Getting all objects: ";

}
