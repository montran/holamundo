package edu.montran.catalog.manager;

import java.util.List;

import org.springframework.stereotype.Repository;

import edu.montran.catalog.persistance.*;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class PersistanceManager.
 */
@Repository
public final class PersistanceManager {

	/** The persistance behaviour. */
	private PersistanceBehaviour persistanceBehaviour;
	
	/**
	 * Instantiates a new persistance manager.
	 */
	public PersistanceManager()
	{
		persistanceBehaviour = new BDDBehaviour();
	}

	/**
	 * Creates the.
	 *
	 * @param entity the entity
	 */
	public void create(Object entity) {
		persistanceBehaviour.create(entity);
	}

	/**
	 * Find by id.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @param id the id
	 * @return the t
	 */
	public <T> T findById(Class<T> entityClass, long id) {

		return persistanceBehaviour.findById(entityClass, id);
	}

	/**
	 * Delete.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @param id the id
	 * @throws PersistanceException the persistance exception
	 */
	public <T> void delete(Class<T> entityClass, long id) throws PersistanceException {
		persistanceBehaviour.delete(entityClass, id);
	}

	/**
	 * Update.
	 *
	 * @param entity the entity
	 * @throws PersistanceException the persistance exception
	 */
	public void update(Object entity) throws PersistanceException {
		persistanceBehaviour.update(entity);
	}

	/**
	 * Find all.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @return the list
	 */
	public <T> List<T> findAll(Class<T> entityClass) {

		return persistanceBehaviour.findAll(entityClass);

	}

	/**
	 * Find by criteria.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @param columnName the column name
	 * @param valueToSearch the value to search
	 * @return the list
	 */
	public <T> List<T> findByCriteria(Class<T> entityClass, String columnName, Object valueToSearch) {

		return persistanceBehaviour.findByCriteria(entityClass, columnName, valueToSearch);

	}
}
