package edu.montran.catalog.manager;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.montran.catalog.manager.utilities.Utilities;
import edu.montran.catalog.money.Currency;
import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Class CurrencyManager.
 */
@Component
public final class CurrencyManager {

	/** The Constant Log. */
	private static final Logger Log = LogManager.getLogger(CurrencyManager.class);

	/** The persistence manager. */
	@Autowired
	private PersistanceManager persistanceManager;

	/**
	 * Adds the currency.
	 *
	 * @param symbol the symbol
	 * @param name the name
	 * @return the long
	 */
	public long addCurrency(String symbol, String name) {

		Log.info(Utilities.INSERT_MESSAGE + Currency.class);
		Log.info("Params: symbol = " + symbol + "name = " + name);

		Currency currency = new Currency(symbol, name);
		persistanceManager.create(currency);

		return currency.getId();

	}

	/**
	 * Adds the currency.
	 *
	 * @param currency the currency
	 * @return the long
	 */
	public long addCurrency(Currency currency) {

		Log.info(Utilities.INSERT_MESSAGE + currency);

		persistanceManager.create(currency);

		return currency.getId();

	}

	/**
	 * Removes the currency.
	 *
	 * @param id the id
	 * @throws PersistanceException the persistence exception
	 */
	public void removeCurrency(long id) throws PersistanceException {
		Log.info(Utilities.DELETE_MESSAGE + Currency.class);
		Log.info("Id: " + id);

		persistanceManager.delete(Currency.class, id);
	}

	/**
	 * Update currency.
	 *
	 * @param id the id
	 * @param symbol the symbol
	 * @param name the name
	 * @throws PersistanceException the persistence exception
	 */
	public void updateCurrency(long id, String symbol, String name) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + Currency.class);
		Log.info("Id: " + id);

		Currency currency = persistanceManager.findById(Currency.class, id);
		currency.setName(name);
		currency.setSymbol(symbol);
		persistanceManager.update(currency);
	}

	/**
	 * Update currency.
	 *
	 * @param currency the currency
	 * @throws PersistanceException the persistance exception
	 */
	public void updateCurrency(Currency currency) throws PersistanceException {

		Log.info(Utilities.UPDATE_MESSAGE + Currency.class);
		Log.info("Id: " + currency.getId());
		persistanceManager.update(currency);
	}

	/**
	 * Find currency by id.
	 *
	 * @param id the id
	 * @return the currency
	 */
	public Currency findCurrencyById(long id) {

		Log.info(Utilities.SEARCH_MESSAGE + Currency.class);
		Log.info("Id: " + id);

		return persistanceManager.findById(Currency.class, id);
	}

	/**
	 * Gets the all currencies.
	 *
	 * @return the all currencies
	 */
	public List<Currency> getAllCurrecies() {

		Log.info(Utilities.GET_ALL_MESSAGE + Currency.class);

		return persistanceManager.findAll(Currency.class);
	}
}