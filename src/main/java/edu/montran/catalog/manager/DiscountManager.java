package edu.montran.catalog.manager;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.montran.catalog.discount.AbstractDiscount;
import edu.montran.catalog.discount.AgeDiscount;
import edu.montran.catalog.discount.GroupDiscount;
import edu.montran.catalog.discount.VolumeDiscount;
import edu.montran.catalog.manager.utilities.Utilities;
import edu.montran.catalog.money.Currency;
import edu.montran.catalog.persistance.exception.PersistanceException;
import edu.montran.catalog.stock.Group;
import edu.montran.catalog.stock.Product;

/**
 * The Class DiscountManager.
 */
@Component
public final class DiscountManager {
	
	/** The stock manager. */
	@Autowired
	private StockManager stockManager;
	
	/** The persistance manager. */
	@Autowired
	private PersistanceManager persistanceManager;

	/** The Constant Log. */
	private static final Logger Log = LogManager.getLogger(DiscountManager.class);

	/**
	 * Adds the volume discount.
	 *
	 * @param name the name
	 * @param minQuantity the min quantity
	 * @param maxQuantity the max quantity
	 * @param starDate the star date
	 * @param endDate the end date
	 * @return the long
	 */
	public long addVolumeDiscount(String name, int minQuantity, int maxQuantity, String starDate, String endDate, float disc) {

		VolumeDiscount volumeDiscount = new VolumeDiscount();

		volumeDiscount.setName(name);
		volumeDiscount.setMinQuantity(minQuantity);
		volumeDiscount.setMaxQuantity(maxQuantity);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		formatter = formatter.withLocale(Locale.US);

		LocalDate startLocalDate = LocalDate.parse(starDate, formatter);
		LocalDate endLocalDate = LocalDate.parse(endDate, formatter);

		volumeDiscount.setStartDate(startLocalDate);
		volumeDiscount.setEndDate(endLocalDate);
		volumeDiscount.setPercentage(disc);

		Log.info(Utilities.INSERT_MESSAGE + volumeDiscount);

		persistanceManager.create(volumeDiscount);

		return volumeDiscount.getId();
	}

	/**
	 * Adds the volume discount.
	 *
	 * @param volumeDiscount the volume discount
	 * @return the long
	 */
	public long addVolumeDiscount(VolumeDiscount volumeDiscount) {

		Log.info(Utilities.INSERT_MESSAGE + volumeDiscount);
		persistanceManager.create(volumeDiscount);

		return volumeDiscount.getId();
	}

	/**
	 * Adds the group discount.
	 *
	 * @param groupDiscount the group discount
	 * @return the long
	 */
	public long addGroupDiscount(GroupDiscount groupDiscount) {

		Log.info(Utilities.INSERT_MESSAGE + groupDiscount);
		persistanceManager.create(groupDiscount);

		return groupDiscount.getId();
	}

	/**
	 * Adds the age discount.
	 *
	 * @param starDate the star date
	 * @param endDate the end date
	 * @param maxDate the max date
	 * @param percentage the percentage
	 * @return the long
	 */
	public long addAgeDiscount(String name, String starDate, String endDate, String maxDate, float percentage) {

		AgeDiscount ageDiscount = new AgeDiscount();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		formatter = formatter.withLocale(Locale.US);

		LocalDate startLocalDate = LocalDate.parse(starDate, formatter);
		LocalDate endLocalDate = LocalDate.parse(endDate, formatter);
		LocalDate maxLocalDate = LocalDate.parse(maxDate, formatter);

		ageDiscount.setStartDate(startLocalDate);
		ageDiscount.setEndDate(endLocalDate);
		ageDiscount.setMaxDate(maxLocalDate);
		ageDiscount.setPercentage(percentage);
		ageDiscount.setName(name);

		Log.info(Utilities.INSERT_MESSAGE + ageDiscount);

		persistanceManager.create(ageDiscount);

		return ageDiscount.getId();
	}

	/**
	 * Adds the age discount.
	 *
	 * @param ageDiscount the age discount
	 * @return the long
	 */
	public long addAgeDiscount(AgeDiscount ageDiscount) {

		Log.info(Utilities.INSERT_MESSAGE + ageDiscount);

		persistanceManager.create(ageDiscount);

		return ageDiscount.getId();
	}

	/**
	 * Adds the discount to product.
	 *
	 * @param discountId the discount id
	 * @param productId the product id
	 * @throws PersistanceException the persistance exception
	 */
	public void addDiscountToProduct(long discountId, long productId) throws PersistanceException {

		AbstractDiscount discount = findDiscountById(discountId);
		Product product = stockManager.findProductById(productId);

		discount.addProduct(product);
		product.addDiscount(discount);

		Log.info("Adding discount to product");
		Log.info("Params: Product Id = " + productId + "Discount Id = " + discountId);

		persistanceManager.update(discount);

	}

	/**
	 * Adds the discount to group.
	 *
	 * @param discountId the discount id
	 * @param groupId the group id
	 * @throws PersistanceException the persistance exception
	 */
	public void addDiscountToGroup(long discountId, long groupId) throws PersistanceException {

		GroupDiscount discount = findGroupDiscountById(discountId);
		Group group = stockManager.findGroupById(groupId);

		discount.addGroup(group);
		group.addDiscount(discount);

		Log.info("Adding discount to group");
		Log.info("Params: Group Id = " + groupId + "Discount Id = " + discountId);

		persistanceManager.update(discount);

	}

	/**
	 * Removes the discount.
	 *
	 * @param discountId the discount id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeDiscount(long discountId) throws PersistanceException {

		Log.info(Utilities.DELETE_MESSAGE + AbstractDiscount.class);
		Log.info("Id: " + discountId);

		persistanceManager.delete(AbstractDiscount.class, discountId);
	}

	/**
	 * Removes the discount from product.
	 *
	 * @param discountId the discount id
	 * @param productId the product id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeDiscountFromProduct(long discountId, long productId) throws PersistanceException {

		Log.info("Removing discount from product");
		Log.info("Product Id = " + productId + "Discount Id = " + discountId);

		Product product = stockManager.findProductById(productId);

		for (int i = 0; i < product.getDiscountCount(); i++) {
			AbstractDiscount discount = product.getDiscount(i);

			if (discountId == discount.getId()) {
				product.removeDiscount(i);
				break;
			}

		}

		persistanceManager.update(product);

	}

	/**
	 * Removes the discount from group.
	 *
	 * @param discountId the discount id
	 * @param groupId the group id
	 * @throws PersistanceException the persistance exception
	 */
	public void removeDiscountFromGroup(long discountId, long groupId) throws PersistanceException {

		Log.info("Removing discount from group");
		Log.info("Product Id = " + discountId + "Discount Id = " + discountId);

		Group group = stockManager.findGroupById(groupId);

		for (int i = 0; i < group.getDiscountCount(); i++) {
			AbstractDiscount discount = group.getDiscount(i);

			if (discountId == discount.getId()) {
				group.removeDiscount(i);
				break;
			}

		}

		persistanceManager.update(group);

	}

	/**
	 * Find volume discount by id.
	 *
	 * @param id the id
	 * @return the volume discount
	 */
	public VolumeDiscount findVolumeDiscountById(long id) {
		
		Log.info(Utilities.SEARCH_MESSAGE + VolumeDiscount.class);
		Log.info("Id: " + id);
		
		return persistanceManager.findById(VolumeDiscount.class, id);
	}

	/**
	 * Find group discount by id.
	 *
	 * @param id the id
	 * @return the group discount
	 */
	public GroupDiscount findGroupDiscountById(long id) {
		
		Log.info(Utilities.SEARCH_MESSAGE + Currency.class);
		Log.info("Id: " + id);
		
		return persistanceManager.findById(GroupDiscount.class, id);
	}

	/**
	 * Find discount by id.
	 *
	 * @param id the id
	 * @return the abstract discount
	 */
	public AbstractDiscount findDiscountById(long id) {
		
		Log.info(Utilities.SEARCH_MESSAGE + AbstractDiscount.class);
		Log.info("Id: " + id);
		
		return persistanceManager.findById(AbstractDiscount.class, id);
	}

	/**
	 * Gets the all volume discounts.
	 *
	 * @return the all volume discounts
	 */
	public List<VolumeDiscount> getAllVolumeDiscounts() {
		
		Log.info(Utilities.GET_ALL_MESSAGE + VolumeDiscount.class);
		
		return persistanceManager.findAll(VolumeDiscount.class);
	}

	/**
	 * Gets the all age discounts.
	 *
	 * @return the all age discounts
	 */
	public List<AgeDiscount> getAllAgeDiscounts() {
		
		Log.info(Utilities.GET_ALL_MESSAGE + AgeDiscount.class);
		
		return persistanceManager.findAll(AgeDiscount.class);
	}

	/**
	 * Gets the all discounts.
	 *
	 * @return the all discounts
	 */
	public List<AbstractDiscount> getAllDiscounts() {
		
		Log.info(Utilities.GET_ALL_MESSAGE + AbstractDiscount.class);
		
		return persistanceManager.findAll(AbstractDiscount.class);
	}

	/**
	 * Gets the all group discounts.
	 *
	 * @return the all group discounts
	 */
	public List<GroupDiscount> getAllGroupDiscounts() {
		
		Log.info(Utilities.GET_ALL_MESSAGE + GroupDiscount.class);
		
		return persistanceManager.findAll(GroupDiscount.class);
	}

	/**
	 * Edits the volume discount.
	 *
	 * @param discountId the discount id
	 * @param percentage the percentage
	 * @param minQuantity the min quantity
	 * @param maxQuantity the max quantity
	 * @throws PersistanceException the persistance exception
	 */
	public void editVolumeDiscount(long discountId, float percentage, int minQuantity, int maxQuantity)
			throws PersistanceException {
		
		
		VolumeDiscount discount = findVolumeDiscountById(discountId);
		
		Log.info(Utilities.UPDATE_MESSAGE + discount);

		discount.setPercentage(percentage);
		discount.setMinQuantity(minQuantity);
		discount.setMaxQuantity(maxQuantity);
		persistanceManager.update(discount);

	}

	/**
	 * Edits the volume discount.
	 *
	 * @param discount the discount
	 * @throws PersistanceException the persistance exception
	 */
	public void editVolumeDiscount(VolumeDiscount discount) throws PersistanceException {
		
		Log.info(Utilities.UPDATE_MESSAGE + discount);
		persistanceManager.update(discount);

	}

	/**
	 * Edits the age discount.
	 *
	 * @param discount the discount
	 * @throws PersistanceException the persistance exception
	 */
	public void editAgeDiscount(AgeDiscount discount) throws PersistanceException {
		
		Log.info(Utilities.UPDATE_MESSAGE + discount);
		persistanceManager.update(discount);

	}

	/**
	 * Edits the group discount.
	 *
	 * @param discount the discount
	 * @throws PersistanceException the persistance exception
	 */
	public void editGroupDiscount(GroupDiscount discount) throws PersistanceException {
		
		Log.info(Utilities.UPDATE_MESSAGE + discount);
		persistanceManager.update(discount);

	}

}