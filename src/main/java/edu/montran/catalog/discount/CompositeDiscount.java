package edu.montran.catalog.discount;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import edu.montran.catalog.stock.AbstractBuyable;
import edu.montran.catalog.stock.Product;

/**
 * The Class CompositeDiscount.
 */
public class CompositeDiscount extends AbstractDiscount {

	/** The discounts. */
	List<AbstractDiscount> discounts = new ArrayList<>();

	/**
	 * Adds the discount.
	 *
	 * @param discount the discount
	 */
	public void addDiscount(AbstractDiscount discount) {
		discounts.add(discount);
	}

	/**
	 * Compute discount.
	 *
	 * @param item the item
	 * @param quantity the quantity
	 * @return the float
	 */
	@Override
	public float computeDiscount(AbstractBuyable item, int quantity) {

		if (item instanceof Product) {
			Product aux = (Product) item;
			List<GroupDiscount> groupDiscounts = aux.getGroup().getDiscounts();

			for (int i = 0; i < groupDiscounts.size(); i++)
				addDiscount(groupDiscounts.get(i));

		}

		percentage = 0;

		float volumePercent = getVolumeDiscount(quantity);
		float agePercent = getAgeDiscount();
		float groupPercent = getGroupDiscount();

		percentage = volumePercent + agePercent + groupPercent;

		return percentage;
	}

	/**
	 * Gets the volume discount.
	 *
	 * @param quantity the quantity
	 * @return the volume discount
	 */
	public float getVolumeDiscount(int quantity) {
		// Get volume discount
		float volumePercent = 0;
		LocalDate today = LocalDate.now();

		for (int i = 0; i < discounts.size(); i++) {
			if (discounts.get(i) instanceof VolumeDiscount) {
				VolumeDiscount aux = (VolumeDiscount) discounts.get(i);

				int max = aux.getMaxQuantity();
				int min = aux.getMinQuantity();

				if (aux.getStartDate().isEqual(today)
						|| aux.getStartDate().isAfter(today) && aux.getStartDate().isEqual(today)
						|| aux.getEndDate().isBefore(today))
					if (min <= quantity && max >= quantity) {
						volumePercent = aux.getPercentage();
						break;
					}

			}

		}
		return volumePercent;
	}

	/**
	 * Gets the age discount.
	 *
	 * @return the age discount
	 */
	public float getAgeDiscount() {

		LocalDate today = LocalDate.now();
		// Get age discount
		float agePercent = 0;
		for (int i = 0; i < discounts.size(); i++) {
			if (discounts.get(i) instanceof AgeDiscount) {
				AgeDiscount aux = (AgeDiscount) discounts.get(i);

				LocalDate maxDate = aux.getMaxDate();

				if (aux.getStartDate().isEqual(today)
						|| aux.getStartDate().isAfter(today) && aux.getStartDate().isEqual(today)
						|| aux.getEndDate().isBefore(today))
					if (today.isBefore(maxDate) || today.isEqual(maxDate)) {

						agePercent += aux.getPercentage();
					}
			}

		}

		return agePercent;
	}

	/**
	 * Gets the group discount.
	 *
	 * @return the group discount
	 */
	public float getGroupDiscount() {
		LocalDate today = LocalDate.now();

		// Get group discount
		float groupPercent = 0;

		for (int i = 0; i < discounts.size(); i++) {
			if (discounts.get(i) instanceof GroupDiscount) {
				GroupDiscount aux = (GroupDiscount) discounts.get(i);

				if (aux.getStartDate().isEqual(today)
						|| aux.getStartDate().isAfter(today) && aux.getStartDate().isEqual(today)
						|| aux.getEndDate().isBefore(today)) {

					groupPercent += aux.getPercentage();
				}

			}

		}

		return groupPercent;
	}

	/**
	 * Gets the discounts.
	 *
	 * @return the discounts
	 */
	public List<AbstractDiscount> getDiscounts() {
		return discounts;
	}

	/**
	 * Sets the discounts.
	 *
	 * @param discounts the new discounts
	 */
	public void setDiscounts(List<AbstractDiscount> discounts) {
		this.discounts = discounts;
	}

}
