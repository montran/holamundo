package edu.montran.catalog.discount;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import edu.montran.catalog.stock.AbstractBuyable;
import edu.montran.catalog.stock.Group;

/**
 * The Class GroupDiscount.
 */
@Entity
@DiscriminatorValue("Group")
public class GroupDiscount extends AbstractDiscount {

	/** The groups. */
	@ManyToMany(mappedBy = "discounts", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<Group> groups = new ArrayList<>();

	/**
	 * Compute discount.
	 *
	 * @param item     the item
	 * @param quantity the quantity
	 * @return the float
	 */
	@Override
	float computeDiscount(AbstractBuyable item, int quantity) {

		return percentage;
	}

	/**
	 * Adds the group.
	 *
	 * @param group the group
	 */
	public void addGroup(Group group) {
		groups.add(group);
	}

}
