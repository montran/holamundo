package edu.montran.catalog.discount;

import javax.persistence.*;

import edu.montran.catalog.stock.AbstractBuyable;

/**
 * The Class VolumeDiscount.
 */
@Entity
@DiscriminatorValue("Volume")
public class VolumeDiscount extends AbstractDiscount {

	/** The min quantity. */
	@Column(name = "DIS_MIN_QUANTITY")
	int minQuantity;

	/** The max quantity. */
	@Column(name = "DIS_MAX_QUANTITY")
	int maxQuantity;

	/**
	 * Compute discount.
	 *
	 * @param item     the item
	 * @param quantity the quantity
	 * @return the float
	 */
	@Override
	float computeDiscount(AbstractBuyable item, int quantity) {

		if (minQuantity >= quantity && quantity <= maxQuantity) {
			percentage = 0.1f;
		}

		return percentage;
	}

	/**
	 * Gets the min quantity.
	 *
	 * @return the min quantity
	 */
	public int getMinQuantity() {
		return minQuantity;
	}

	/**
	 * Sets the min quantity.
	 *
	 * @param minQuantity the new min quantity
	 */
	public void setMinQuantity(int minQuantity) {
		this.minQuantity = minQuantity;
	}

	/**
	 * Gets the max quantity.
	 *
	 * @return the max quantity
	 */
	public int getMaxQuantity() {
		return maxQuantity;
	}

	/**
	 * Sets the max quantity.
	 *
	 * @param maxQuantity the new max quantity
	 */
	public void setMaxQuantity(int maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "VolumeDiscount [minQuantity=" + minQuantity + ", maxQuantity=" + maxQuantity + ", id=" + id
				+ ", percentage=" + percentage + ", startDate=" + startDate + ", endDate=" + endDate + ", name=" + name
				+ "]";
	}

}
