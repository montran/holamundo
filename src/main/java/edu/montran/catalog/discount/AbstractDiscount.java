package edu.montran.catalog.discount;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import edu.montran.catalog.stock.AbstractBuyable;
import edu.montran.catalog.stock.Product;

/**
 * The Class AbstractDiscount.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DIS_TYPE")
@Table(name = "DISCOUNT")
public abstract class AbstractDiscount {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DIS_ID", updatable = false, nullable = false)
	protected Long id;

	/** The percentage. */
	@Column(name = "DIS_PERCENTAGE")
	protected float percentage;

	/** The products. */
	@ManyToMany(mappedBy = "discounts", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<Product> products = new ArrayList<>();

	/** The start date. */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "DIS_START_DATE")
	LocalDate startDate;

	/** The end date. */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "DIS_END_DATE")
	LocalDate endDate;

	/** The name. */
	@Column(name = "DIS_NAME")
	String name;

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public LocalDate getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	/**
	 * Compute discount.
	 *
	 * @param item the item
	 * @param quantity the quantity
	 * @return the float
	 */
	abstract float computeDiscount(AbstractBuyable item, int quantity);

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Adds the product.
	 *
	 * @param product the product
	 */
	public void addProduct(Product product) {
		products.add(product);
	}

	/**
	 * Gets the products.
	 *
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * Sets the products.
	 *
	 * @param products the new products
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the percentage.
	 *
	 * @return the percentage
	 */
	public float getPercentage() {
		return percentage;
	}

	/**
	 * Sets the percentage.
	 *
	 * @param percentage the new percentage
	 */
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "AbstractDiscount [id=" + id + ", percentage=" + percentage + ", startDate=" + startDate + ", endDate="
				+ endDate + ", name=" + name + "]";
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractDiscount other = (AbstractDiscount) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
