package edu.montran.catalog.discount;

import java.time.LocalDate;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import edu.montran.catalog.stock.AbstractBuyable;

/**
 * The Class AgeDiscount.
 */
@Entity
@DiscriminatorValue("Age")
public class AgeDiscount extends AbstractDiscount {

	/** The max date. */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "DIS_MAX_DATE")
	LocalDate maxDate;

	/**
	 * Compute discount.
	 *
	 * @param item the item
	 * @param quantity the quantity
	 * @return the float
	 */
	@Override
	float computeDiscount(AbstractBuyable item, int quantity) {
		return 0;
	}

	/**
	 * Gets the max date.
	 *
	 * @return the max date
	 */
	public LocalDate getMaxDate() {
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate the new max date
	 */
	public void setMaxDate(LocalDate maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "AgeDiscount [maxDate=" + maxDate + ", id=" + id + ", percentage=" + percentage + ", startDate="
				+ startDate + ", endDate=" + endDate + ", name=" + name + "]";
	}

}
