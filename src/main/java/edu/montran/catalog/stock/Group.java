package edu.montran.catalog.stock;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.envers.NotAudited;

import edu.montran.catalog.discount.AbstractDiscount;
import edu.montran.catalog.discount.GroupDiscount;

/**
 * The Class Group.
 */
@Entity
@Table(name = "CATEGORY")
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class Group {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CAT_ID")
	private long id;

	/** The name. */
	@Column(name = "CAT_NAME")
	String name;

	/** The description. */
	@Column(name = "CAT_DESCRIPTION")
	String description;

	/** The products. */
	@OneToMany(mappedBy = "group", cascade = CascadeType.MERGE)
	List<Product> products = new ArrayList<>();

	/** The discounts. */
	@NotAudited
	@ManyToMany(fetch = FetchType.EAGER)
	List<GroupDiscount> discounts = new ArrayList<>();

	/**
	 * Instantiates a new group.
	 *
	 * @param name the name
	 * @param description the description
	 */
	public Group(String name, String description) {
		this.name = name;
		this.description = description;
	}

	/**
	 * Instantiates a new group.
	 */
	public Group() {

	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the discounts.
	 *
	 * @return the discounts
	 */
	public List<GroupDiscount> getDiscounts() {
		return discounts;
	}

	/**
	 * Sets the discounts.
	 *
	 * @param discounts the new discounts
	 */
	public void setDiscounts(List<GroupDiscount> discounts) {
		this.discounts = discounts;
	}

	/**
	 * Adds the discount.
	 *
	 * @param discount the discount
	 */
	public void addDiscount(GroupDiscount discount) {
		discounts.add(discount);
	}

	/**
	 * Removes the discount.
	 *
	 * @param index the index
	 */
	public void removeDiscount(int index) {
		discounts.remove(index);
	}

	/**
	 * Gets the discount count.
	 *
	 * @return the discount count
	 */
	public int getDiscountCount() {
		return discounts.size();
	}

	/**
	 * Gets the discount.
	 *
	 * @param index the index
	 * @return the discount
	 */
	public AbstractDiscount getDiscount(int index) {
		return discounts.get(index);
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", description=" + description + ", products=" + products
				+ ", discounts=" + discounts + "]";
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	

}
