package edu.montran.catalog.stock;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class Bundle.
 */
@Entity
@Table(name = "BUNDLE")
public class Bundle extends AbstractBuyable {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "BUND_ID")
	private long id;

	/** The products per bundle. */
	// Intermediate table
	@OneToMany(mappedBy = "bundle", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<ProductsPerBundle> productsPerBundle = new ArrayList<>();

	/**
	 * Instantiates a new bundle.
	 *
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 */
	public Bundle(String shortName, String longName, float price) {
		super(shortName, longName, price);
	}

	/**
	 * Instantiates a new bundle.
	 */
	public Bundle() {

	}

	/**
	 * Adds the products per bundle.
	 *
	 * @param productPerBundle the product per bundle
	 */
	public void addProductsPerBundle(ProductsPerBundle productPerBundle) {
		this.productsPerBundle.add(productPerBundle);
	}

	/**
	 * Clear products per bundle.
	 */
	public void clearProductsPerBundle() {
		productsPerBundle.clear();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the products per bundle.
	 *
	 * @return the products per bundle
	 */
	public List<ProductsPerBundle> getProductsPerBundle() {
		return productsPerBundle;
	}

	/**
	 * Sets the products per bundle.
	 *
	 * @param productsPerBundle the new products per bundle
	 */
	public void setProductsPerBundle(List<ProductsPerBundle> productsPerBundle) {
		this.productsPerBundle = productsPerBundle;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Bundle [id=" + id + ", productsPerBundle=" + productsPerBundle + "]";
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bundle other = (Bundle) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	

}
