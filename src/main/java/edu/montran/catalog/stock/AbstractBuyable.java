package edu.montran.catalog.stock;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.envers.Audited;

/**
 * The Class AbstractBuyable.
 */
@MappedSuperclass
@Audited
public abstract class AbstractBuyable {

	/** The short name. */
	@Column(name = "BUYA_SHORT_NAME")
	protected String shortName;

	/** The long name. */
	@Column(name = "BUYA_LONG_NAME")
	protected String longName;

	/** The price. */
	@Column(name = "BUYA_PRICE")
	protected float price;

	/**
	 * Instantiates a new abstract buyable.
	 *
	 * @param shortName the short name
	 * @param longName  the long name
	 * @param price     the price
	 */
	public AbstractBuyable(String shortName, String longName, float price) {
		this.shortName = shortName;
		this.longName = longName;
		this.price = price;
	}

	/**
	 * Instantiates a new abstract buyable.
	 */
	public AbstractBuyable() {
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Gets the short name.
	 *
	 * @return the short name
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Sets the short name.
	 *
	 * @param shortName the new short name
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Gets the long name.
	 *
	 * @return the long name
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * Sets the long name.
	 *
	 * @param longName the new long name
	 */
	public void setLongName(String longName) {
		this.longName = longName;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "AbstractBuyable [shortName=" + shortName + ", longName=" + longName + ", price=" + price + "]";
	}

}
