package edu.montran.catalog.stock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.format.annotation.DateTimeFormat;

import edu.montran.catalog.discount.AbstractDiscount;
import edu.montran.catalog.money.Currency;

/**
 * The Class Product.
 */
@Entity
@Table(name = "PRODUCT")
@Audited
public class Product extends AbstractBuyable {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PROD_ID")
	private long id;

	/** The stock. */
	@Column(name = "PROD_STOCK")
	int stock;

	/** The keywords. */
	@Column(name = "PROD_KEYWORDS")
	String keywords;

	/** The creation date. */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "PROD_CREATION_DATE")
	LocalDate creationDate;

	/** The currency. */
	@NotAudited
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CURR_ID")
	protected Currency currency;

	/** The group. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CAT_ID")
	protected Group group;

	/** The atributes. */
	@NotAudited
	@OneToMany(mappedBy = "product", cascade = CascadeType.MERGE)
	List<Atribute> atributes = new ArrayList<>();

	/** The products per bundle. */
	@NotAudited
	// intermediate table
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProductsPerBundle> productsPerBundle  = new ArrayList<>();

	/** The discounts. */
	@NotAudited
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<AbstractDiscount> discounts = new ArrayList<>();

	/**
	 * Instantiates a new product.
	 *
	 * @param shortName the short name
	 * @param longName the long name
	 * @param price the price
	 * @param stock the stock
	 */
	public Product(String shortName, String longName, float price, int stock) {
		super(shortName, longName, price);
		this.stock = stock;
	}

	/**
	 * Instantiates a new product.
	 */
	public Product() {

	}

	/**
	 * Adds the discount.
	 *
	 * @param discount the discount
	 */
	public void addDiscount(AbstractDiscount discount) {
		discounts.add(discount);
	}

	/**
	 * Adds the products per bundle.
	 *
	 * @param productPerBundle the product per bundle
	 */
	public void addProductsPerBundle(ProductsPerBundle productPerBundle) {
		this.productsPerBundle.add(productPerBundle);
	}

	/**
	 * Removes the discount.
	 *
	 * @param index the index
	 */
	public void removeDiscount(int index) {
		discounts.remove(index);
	}

	/**
	 * Gets the discount count.
	 *
	 * @return the discount count
	 */
	public int getDiscountCount() {
		return discounts.size();
	}

	/**
	 * Gets the discount.
	 *
	 * @param index the index
	 * @return the discount
	 */
	public AbstractDiscount getDiscount(int index) {
		return discounts.get(index);
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * Gets the group.
	 *
	 * @return the group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * Sets the group.
	 *
	 * @param group the new group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * Gets the stock.
	 *
	 * @return the stock
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * Sets the stock.
	 *
	 * @param stock the new stock
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * Gets the products per bundle.
	 *
	 * @return the products per bundle
	 */
	public List<ProductsPerBundle> getProductsPerBundle() {
		return productsPerBundle;
	}

	/**
	 * Sets the products per bundle.
	 *
	 * @param productsPerBundle the new products per bundle
	 */
	public void setProductsPerBundle(List<ProductsPerBundle> productsPerBundle) {
		this.productsPerBundle = productsPerBundle;
	}

	/**
	 * Gets the discounts.
	 *
	 * @return the discounts
	 */
	public List<AbstractDiscount> getDiscounts() {
		return discounts;
	}

	/**
	 * Sets the discounts.
	 *
	 * @param discounts the new discounts
	 */
	public void setDiscounts(List<AbstractDiscount> discounts) {
		this.discounts = discounts;
	}

	/**
	 * Gets the keywords.
	 *
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * Sets the keywords.
	 *
	 * @param keywords the new keywords
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public LocalDate getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the atributes.
	 *
	 * @return the atributes
	 */
	public List<Atribute> getAtributes() {
		return atributes;
	}

	/**
	 * Sets the atributes.
	 *
	 * @param atributes the new atributes
	 */
	public void setAtributes(List<Atribute> atributes) {
		this.atributes = atributes;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Product [id=" + id + ", stock=" + stock + ", keywords=" + keywords + ", creationDate=" + creationDate + "]";
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}
