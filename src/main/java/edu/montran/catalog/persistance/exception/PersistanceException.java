package edu.montran.catalog.persistance.exception;

/**
 * The Class PersistanceException.
 */
public class PersistanceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4301025342308921383L;

	/**
	 * Instantiates a new persistance exception.
	 *
	 * @param message the message
	 */
	public PersistanceException(String message) {
		this.message = message;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return message;
	}

	/** The message. */
	String message;

}
