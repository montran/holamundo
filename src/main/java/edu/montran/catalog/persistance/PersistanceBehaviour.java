package edu.montran.catalog.persistance;

import java.util.List;

import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * The Interface PersistanceBehaviour.
 */
public interface PersistanceBehaviour {

	/**
	 * Creates the.
	 *
	 * @param entity the entity
	 */
	public void create(Object entity);

	/**
	 * Update.
	 *
	 * @param entity the entity
	 * @throws PersistanceException the persistance exception
	 */
	public void update(Object entity) throws PersistanceException;

	/**
	 * Delete.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @param id the id
	 * @throws PersistanceException the persistence exception
	 */
	public <T> void delete(Class<T> entityClass, long id) throws PersistanceException;

	/**
	 * Find by id.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @param id the id
	 * @return the t
	 */
	public <T> T findById(Class<T> entityClass, long id);

	/**
	 * Find all.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @return the list
	 */
	public <T> List<T> findAll(Class<T> entityClass);

	/**
	 * Find by criteria.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @param columnName the column name
	 * @param valueToSearch the value to search
	 * @return the list
	 */
	public <T> List<T> findByCriteria(Class<T> entityClass, String columnName, Object valueToSearch);

}
