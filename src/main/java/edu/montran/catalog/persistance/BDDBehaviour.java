package edu.montran.catalog.persistance;

import java.util.List;

import javax.persistence.*;
import javax.persistence.criteria.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.montran.catalog.persistance.exception.PersistanceException;

/**
 * Class BDDBehaviour. Generic database persistence class
 */
public class BDDBehaviour implements PersistanceBehaviour {

	/** The Constant Log. */
	private static final Logger Log = LogManager.getLogger(BDDBehaviour.class);

	/** The Entity Manager Factory. */
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistencia");

	/** The Entity Manager. */
	private static EntityManager manager;

	/**
	 * Creates the.
	 *
	 * @param entity the entity
	 */
	@Override
	public void create(Object entity) {

		try {
			manager = emf.createEntityManager();
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.getTransaction().commit();
		} catch (Exception e) {
			Log.error(e.getMessage());
			manager.getTransaction().rollback();
		} finally {
			manager.close();
		}

	}

	/**
	 * Update.
	 *
	 * @param entity the entity
	 * @throws PersistanceException
	 */
	@Override
	public void update(Object entity) throws PersistanceException {

		try {
			manager = emf.createEntityManager();
			manager.getTransaction().begin();
			manager.merge(entity);
			manager.getTransaction().commit();
		} catch (Exception e) {
			Log.error(e.getMessage());
			manager.getTransaction().rollback();
			throw new PersistanceException("The entity cannot be updated");
		} finally {
			manager.close();
		}

	}

	/**
	 * Find by id.
	 *
	 * @param <T>         the generic type
	 * @param entityClass the entity class
	 * @param id          the id
	 * @return the t
	 */
	@Override
	public <T> T findById(Class<T> entityClass, long id) {

		manager = emf.createEntityManager();
		T aux = manager.find(entityClass, id);
		manager.close();

		return aux;
	}

	/**
	 * Delete.
	 *
	 * @param <T>         the generic type
	 * @param entityClass the entity class
	 * @param id          the id
	 * @throws PersistanceException
	 */
	@Override
	public <T> void delete(Class<T> entityClass, long id) throws PersistanceException {

		try {
			manager = emf.createEntityManager();
			T aux = manager.find(entityClass, id);

			if (aux != null) {
				manager.getTransaction().begin();
				manager.remove(aux);
				manager.getTransaction().commit();
			} else {
				Log.warn("Trying to delete unexistent object of type: " + entityClass.getClass());
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
			manager.getTransaction().rollback();
			throw new PersistanceException("The entity has active relationships and cannot be deleted");
		} finally {
			manager.close();
		}

	}

	/**
	 * Find all.
	 *
	 * @param <T>         the generic type
	 * @param entityClass the entity class
	 * @return the list
	 */
	@Override
	public <T> List<T> findAll(Class<T> entityClass) {

		manager = emf.createEntityManager();
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(entityClass);
		Root<T> root = query.from(entityClass);
		query.select(root);
		List<T> aux = manager.createQuery(query).getResultList();
		manager.close();

		return aux;
	}

	public <T> List<T> findByCriteria(Class<T> entityClass, String columnName, Object valueToSearch) {

		manager = emf.createEntityManager();
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(entityClass);
		Root<T> root = query.from(entityClass);
		query.select(root).where(builder.equal(root.get(columnName), valueToSearch));

		List<T> aux = manager.createQuery(query).getResultList();
		manager.close();

		return aux;
	}

}
