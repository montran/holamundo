$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
        var productId = $(this).data("productid");
				$("#deleteForm").attr("action","/HelloWorld/attribute/delete/"+objectId+"/"+productId);
    });

		$(".edit").click(function(){
				var attribId = $(this).data("id");
				var attribName = $(this).data("name");
				var attribType = $(this).data("type");
        var attribValue = $(this).data("value");
        var attribProdId = $(this).data("productid");


				$("#updateAttribId").attr("value",attribId);
				$("#updateName").attr("value",attribName);
				$("#updateType").attr("value",attribType);
        $("#updateValue").attr("value",attribValue);
        $("#updateProdId").attr("value",attribProdId);
    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/attribute/update/");
    });


});
