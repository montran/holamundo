$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
				$("#deleteForm").attr("action","/HelloWorld/volume/delete/"+objectId);
    });

		$(".edit").click(function(){
				var volumeId = $(this).data("id");
				var volumeName = $(this).data("name");
        var volumePercentage = $(this).data("percentage");
        var volumeStartDate = $(this).data("startdate");
        var volumeEndDate = $(this).data("enddate");
        var volumeMinQuantity = $(this).data("minquantity");
        var volumeMaxQuantity = $(this).data("maxquantity");


        $("#updateId").attr("value",volumeId);
				$("#updateName").attr("value",volumeName);
        $("#updatePercentage").attr("value",volumePercentage);
        $("#updateStartDate").attr("value",volumeStartDate);
        $("#updateEndDate").attr("value",volumeEndDate);
        $("#updateMinQuan").attr("value",volumeMinQuantity);
        $("#updateMaxQuan").attr("value",volumeMaxQuantity);
    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/volume/update/");
    });


});
