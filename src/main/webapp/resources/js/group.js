$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
				$("#deleteForm").attr("action","/HelloWorld/group/delete/"+objectId);
    });

		$(".edit").click(function(){
				var groupId = $(this).data("id");
				var groupName = $(this).data("name");
				var groupDesc = $(this).data("desc");
				$("#updateId").attr("value",groupId);
				$("#updateName").attr("value",groupName);
        $("#updateDescription").html(groupDesc);
    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/group/update/");
    });


});
