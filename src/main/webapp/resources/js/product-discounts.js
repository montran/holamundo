$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
        var prodId = $(this).data("prodid");

				$("#deleteForm").attr("action","/HelloWorld/product/discount/delete/"+objectId+"/"+prodId);
    });

		$(".edit").click(function(){
				var volumeId = $(this).data("id");
				var volumeName = $(this).data("name");
        var volumePercentage = $(this).data("percentage");
        var volumeStartDate = $(this).data("startdate");
        var volumeEndDate = $(this).data("enddate");
        var volumeMinQuantity = $(this).data("minquantity");
        var volumeMaxDate = $(this).data("maxdate");


        $("#updateId").attr("value",volumeId);
				$("#updateName").attr("value",volumeName);
        $("#updatePercentage").attr("value",volumePercentage);
        $("#updateStartDate").attr("value",volumeStartDate);
        $("#updateEndDate").attr("value",volumeEndDate);
        $("#updateMaxDate").attr("value",volumeMaxDate);

    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/age/update/");
    });


});
