var select = new SlimSelect({
  select: '#select'
});

$(document).ready(function() {

  $(".delete").click(function() {
    var objectId = $(this).data("id");
    $("#deleteForm").attr("action", "/HelloWorld/bundle/delete/" + objectId);
  });

  $(".edit").click(function() {
    var bundleId = $(this).data("id");
    var bundleShortName = $(this).data("shortname");
    var bundleLongName = $(this).data("longname");
    var bundlePrice = $(this).data("price");
    $("#updateId").attr("value", bundleId);
    $("#updateShortName").attr("value", bundleShortName);
    $("#updateLongName").attr("value", bundleLongName);
    $("#updatePrice").attr("value", bundlePrice);
  });

  $(".manage").click(function() {
    var bundleId = $(this).data("id");
    var bundleProducts = $(this).data("products");

      var products = bundleProducts.split("->");

      var index = products.indexOf("");    // <-- Not supported in <IE9
      if (index !== -1) {
          products.splice(index, 1);
      }

      var productCount = products.length/3;

      var selectedProducts=[];
      var selectedProductsNames=[];
      var selectedProductsQuantities=[];
      var j=0;
      //var productsCount = products[0];
      if(productCount>0)
        for(var i=0;i<productCount*3;i+=3)
        {
          console.log("id "+products[i]+" name"+products[i+1]);
          selectedProducts[j]=products[i];
          selectedProductsNames[j] = products[i+1];
          selectedProductsQuantities[j] = products[i+2];
          j++;
        }

      select.set(selectedProducts);

    $("#manageId").attr("value", bundleId);

    $('#quantityTable').html("");
    for(var i=0;i<productCount;i++)
    {
      $('#quantityTable').append("<tr><td ><input type='hidden' name='productid' value='"+selectedProducts[i]+"'>"+selectedProductsNames[i]+"</td><td><input type='number' name='quantity' value='"+selectedProductsQuantities[i]+"'class='form-control'required></td></tr>");
    }
  });

  $("#updateButton").click(function() {
    $("#updateForm").attr("action", "/HelloWorld/bundle/update/");
  });

  $("#addProductButton").click(function() {

    var selectedItems = select.selected();
    var selectedItemsCount = selectedItems.length;
    var seletectionString = "";

    $('#quantityTable').html("");

    for (var i = 0; i < selectedItemsCount; i++)
    {
      seletectionString += selectedItems[i] + "->";
      var option = '#option'+i;
      var name = $(option).data("name");

      $('#quantityTable').append("<tr><td ><input type='hidden' name='productid' value='"+selectedItems[i]+"'>"+name+"</td><td><input type='number' name='quantity' class='form-control'required></td></tr>");
    }

  });


});
