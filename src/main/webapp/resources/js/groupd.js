$(document).ready(function() {
    $(".delete").click(function(){
				var objectId = $(this).data("id");
				$("#deleteForm").attr("action","/HelloWorld/groupd/delete/"+objectId);
    });

		$(".edit").click(function(){
				var volumeId = $(this).data("id");
				var volumeName = $(this).data("name");
        var volumePercentage = $(this).data("percentage");
        var volumeStartDate = $(this).data("startdate");
        var volumeEndDate = $(this).data("enddate");


        $("#updateId").attr("value",volumeId);
				$("#updateName").attr("value",volumeName);
        $("#updatePercentage").attr("value",volumePercentage);
        $("#updateStartDate").attr("value",volumeStartDate);
        $("#updateEndDate").attr("value",volumeEndDate);


    });

		$("#updateButton").click(function(){
				$("#updateForm").attr("action","/HelloWorld/groupd/update/");
    });


});
