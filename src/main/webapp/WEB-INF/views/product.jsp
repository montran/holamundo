<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@include file="./header.jsp"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Manage Products</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<spring:url value="/resources/css/style.css" var="styleCSS" />
<link href="${styleCSS}" rel="stylesheet" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<spring:url value="/resources/js/product.js" var="crudJS" />
<script src="${crudJS}"></script>

</head>

<body>
	<div class="container">
		<%@include file="./message.jsp"%>
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
						<h2>
							Manage <b>Products</b>
						</h2>
					</div>
					<div class="col-sm-6">
						<a href="#addModal" class="btn btn-success" data-toggle="modal">
							<i class="material-icons">&#xE147;</i> <span>Add</span>
						</a>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Id</th>
						<th>Short Name</th>
						<th>Long Name</th>
						<th>Creation Date</th>
						<th>Price</th>
						<th>Stock</th>
						<th>Currency</th>
						<th>Keywords</th>
						<th>Group</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="prod" items="${productList}">
						<tr>
							<td>${prod.id}</td>
							<td>${prod.shortName}</td>
							<td>${prod.longName}</td>
							<td>${prod.creationDate}</td>
							<td>${prod.price}</td>
							<td>${prod.stock}</td>
							<td>${prod.currency.name}</td>
							<td>${prod.keywords}</td>
							<td>${prod.group.name}</td>
							<td class="text-right">
								<a href="/HelloWorld/product/discount/${prod.id}" class="manage" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Add/Remove Discount">&#xe54e;</i></a>
								<a href="#compositeModal" data-id="${prod.id}" class="totalDisc" data-toggle="modal"> <i class="material-icons" data-toggle="tooltip" title="Compute Discount">&#xe854;</i></a>
								<a href="/HelloWorld/attribute/${prod.id}" class="manage" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Add/Remove Atributes">&#xe8b8;</i></a>
								<a href="#editModal" data-id="${prod.id}" data-shortname="${prod.shortName}" data-longname="${prod.longName}" data-price="${prod.price}" data-date="${prod.creationDate}" data-keywords="${prod.keywords}" data-stock="${prod.stock}"
									data-currency="${prod.currency.id}" data-group="${prod.group.id}" class="edit" data-toggle="modal">
									<i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
								<a href="#deleteModal" data-id="${prod.id}" class="delete" data-toggle="modal"> <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	
	<!-- Compute Discount Modal HTML -->
	<div id="compositeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="compositeForm" action="/HelloWorld/product/add/" method="post">
					<div class="modal-header">
						<h4 class="modal-title">Get Total Product Discount</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						
						<div class="form-group" style="display: none">
							<label>Id</label> <input id="compositeId" type="text" name="id"
								class="form-control" required="required">
						</div>
						
						<div class="form-group">
							<label>Quantity</label> <input id="compoisteQuantity" type="text" name="keywords" class="form-control" required="required">
						</div>

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel"> <input id="compositeButton" type="submit"
							class="btn btn-success" value="Add">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Add Modal HTML -->
	<div id="addModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="addForm" action="/HelloWorld/product/add/" method="post">
					<div class="modal-header">
						<h4 class="modal-title">Add Product</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Short Name</label> <input type="text" name="shortName"
								class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Name</label> <input type="text" name="longName"
								class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Price</label> <input type="number" step="any" name="price"
								class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Stock</label> <input type="number" name="stock"
								class="form-control" required="required">
						</div>

						<div class="form-group">
							<label>Currency</label> <select name="currency"
								class="form-control">
								<c:forEach var="curr" items="${currencyList}">
									<option value="${curr.id}">${curr.symbol}${curr.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label>Group</label> <select name="group" class="form-control">
								<c:forEach var="group" items="${groupList}">
									<option value="${group.id}">${group.name}</option>
								</c:forEach>
							</select>
						</div>
						
						<div class="form-group">
							<label>Keywords</label> <input type="text" name="keywords"
								class="form-control" required="required">
						</div>

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel"> <input id="addButton" type="submit"
							class="btn btn-success" value="Add">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Edit Modal HTML -->
	<div id="editModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="updateForm" action="#" method="post">
					<div class="modal-header">
						<h4 class="modal-title">Edit Product</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group" style="display: none">
							<label>Id</label> <input id="updateId" type="text" name="id"
								class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Short Name</label> <input id="updateShortName" type="text"
								name="shortName" class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Name</label> <input id="updateLongName" type="text"
								name="longName" class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Price</label> <input id="updatePrice" type="number"
								step="any" name="price" class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Stock</label> <input id="updateStock" type="number"
								name="stock" class="form-control" required="required">
						</div>

						<div class="form-group">
							<label>Currency</label> <select id="updateCurrency"
								name="currency" class="form-control">
								<c:forEach var="curr" items="${currencyList}">
									<option value="${curr.id}">${curr.symbol}${curr.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label>Group</label> <select id="updateGroup" name="group" class="form-control">
								<c:forEach var="group" items="${groupList}">
									<option value="${group.id}">${group.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label>Keywords</label> <input id="updateKeywords" type="text" name="keywords"
								class="form-control" required="required">
						</div>

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel"> <input id="updateButton" type="submit"
							class="btn btn-info" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Delete Modal HTML -->
	<div id="deleteModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="deleteForm" action="#">
					<div class="modal-header">
						<h4 class="modal-title">Delete Product</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to delete these Records?</p>
						<p class="text-warning">
							<small>This action cannot be undone.</small>
						</p>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel"> <input type="submit"
							class="btn btn-danger" value="Delete" name="objectId">
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

</html>
