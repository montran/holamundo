<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Manage Currency</title>
  
   <spring:url value="/resources/css/slimselect.min.css" var="slimSelectCSS" />
  <link href="${slimSelectCSS}" rel="stylesheet" />
  

  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <spring:url value="/resources/css/style.css" var="styleCSS" />
  <link href="${styleCSS}" rel="stylesheet" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <spring:url value="/resources/js/slimselect.min.js" var="slimSelectJS" />
  <script src="${slimSelectJS}"></script>
  


</head>

<body>
<%@include file="./header.jsp" %>
  <div class="container">
  <%@include file="./message.jsp"%>
    <div class="table-wrapper">
      <div class="table-title">
        <div class="row">
          <div class="col-sm-6">
            <h2>Manage <b>Bundles</b></h2>
          </div>
          <div class="col-sm-6">
            <a href="#addModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add</span></a>
          </div>
        </div>
      </div>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Short Name</th>
            <th>Long Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="bund" items="${list}" varStatus="status">
            <tr>
              <td>${bund.id}</td>
              <td>${bund.shortName}</td>
              <td>${bund.longName}</td>
              <td>${bund.price}</td>
              <td class="text-right">
                <a href="#manageModal" data-id="${bund.id}" data-products="${bundleProducts[status.index]}" class="manage" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Add/Remove Products">&#xe8b8;</i></a>
                <a href="#editModal" data-id="${bund.id}" data-shortname="${bund.shortName}" data-longname="${bund.longName}" data-price="${bund.price}" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                <a href="#deleteModal" data-id="${bund.id}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
              </td>
            </tr>
          </c:forEach>

        </tbody>
      </table>
    </div>
  </div>

  <!-- Add Modal HTML -->
  <div id="addModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="addForm" action="/HelloWorld/bundle/add/" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Add Bundle</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label>Short Name</label>
              <input type="text" name="shortName" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Long Name</label>
              <input type="text" name="longName" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Price</label>
              <input type="number" step="any" name="price" class="form-control" required>
            </div>

          </div>
          <div class="modal-footer">
            <input id="addButton" type="submit" class="btn btn-success" value="Add">
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Edit Modal HTML -->
  <div id="editModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="updateForm" action="#" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Edit Bundle</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-group" style="display: none">
              <label>Id</label>
              <input id="updateId" type="text" name="id" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Short Name</label>
              <input id="updateShortName" type="text" name="shortName" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Long Name</label>
              <input id="updateLongName" type="text" name="longName" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Price</label>
              <input id="updatePrice" type="number" step="any" name="price" class="form-control" required>
            </div>
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="updateButton" type="submit" class="btn btn-info" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Delete Modal HTML -->
  <div id="deleteModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="deleteForm" action="#">
          <div class="modal-header">
            <h4 class="modal-title">Delete Bundle</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete these Records?</p>
            <p class="text-warning"><small>This action cannot be undone.</small></p>
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input type="submit" class="btn btn-danger" value="Delete" name="objectId">
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <!-- Manage Modal HTML -->
  <div id="manageModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="updateForm" action="/HelloWorld/bundle/product/add" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Manage Bundle Products</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">

            
            <div class="form-group" >
            <input id="manageId" type="text" name="id" class="form-control" style="display: none">
	            <select id="select" multiple>
	            <c:set var="count" value="0" scope="page" />
	            <c:forEach var="prod" items="${productList}">
		      			<option id="option${count}" value="${prod.id}" data-name="${prod.longName}">${prod.longName}</option>
		      		<c:set var="count" value="${count + 1}" scope="page"/>
	      		</c:forEach>
	      		
	   			</select>
   			</div>
   			
   			<div class="form-group" >
   				<input id="addProductButton" type="button" class="btn btn-info" value="Add Products">
   			</div>
   			
			<table class="table table-bordered table-hover" >
				<thead>
					<tr >
						<th class="text-center">
							Product
						</th>
						<th class="text-center">
							Quantity
						</th>
					</tr>
				</thead>
				<tbody id="quantityTable">

				</tbody>
			</table>
            
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="manageButton" type="submit" class="btn btn-success" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <spring:url value="/resources/js/bundle.js" var="crudJS" />
  <script src="${crudJS}"></script>
  
</body>

</html>