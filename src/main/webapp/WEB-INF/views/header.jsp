<nav class="navbar navbar-default">
  <div class="container-fluid">
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <a class="navbar-brand" href="#">Catalog Administration</a>
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Stock <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/HelloWorld/group/">Group</a></li>
            <li><a href="/HelloWorld/product/">Products</a></li>
            <li><a href="/HelloWorld/bundle/">Bundles</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Currency <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/HelloWorld/currency/">Currency</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Discounts <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/HelloWorld/volume/">Volume</a></li>
            <li><a href="/HelloWorld/age/">Age</a></li>
            <li><a href="/HelloWorld/groupd/">Group</a></li>
          </ul>
        </li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>